# 4dl
[![Build Status](https://travis-ci.org/fronbasal/4dl.svg?branch=master)](https://travis-ci.org/fronbasal/4dl)
[![GoDoc](https://godoc.org/github.com/fronbasal/4dl?status.svg)](https://godoc.org/github.com/fronbasal/4dl)
[![Go Report Card](https://goreportcard.com/badge/github.com/fronbasal/4dl)](https://goreportcard.com/report/github.com/fronbasal/4dl)

4chan thread download utility

Usage:
```
usage: 4dl [<flags>] <Board> <Thread ID> [<Where to save files>]

Flags:
      --help     Show context-sensitive help (also try --help-long and --help-man).
  -v, --Verbose  Print verbose information

Args:
  <Board>                  Board of the given thread
  <Thread ID>              ID of the thread you want to download
  [<Where to save files>]  Defaults to .

```

The application is not properly testable, because a thread does not stick around.
This is the test output:
```
PASS
ok  	github.com/fronbasal/4dl	0.015s
```
