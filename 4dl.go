package main

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
	"unicode/utf8"

	"strconv"

	"gopkg.in/alecthomas/kingpin.v2"
	pb "gopkg.in/cheggaaa/pb.v1"
)

var (
	verbose = kingpin.Flag("Verbose", "Print verbose information").Short('v').Bool()
	board   = kingpin.Arg("Board", "Board of the given thread").Required().String()
	thread  = kingpin.Arg("Thread ID", "ID of the thread you want to download").Required().String()
	path    = kingpin.Arg("Where to save files", "Defaults to .").Default(".").ExistingDir()
)

// Thread struct for JSON
type Thread struct {
	Posts []struct {
		No          int    `json:"no"`
		Now         string `json:"now"`
		Name        string `json:"name"`
		Com         string `json:"com"`
		Filename    string `json:"filename,omitempty"`
		Ext         string `json:"ext,omitempty"`
		W           int    `json:"w,omitempty"`
		H           int    `json:"h,omitempty"`
		TnW         int    `json:"tn_w,omitempty"`
		TnH         int    `json:"tn_h,omitempty"`
		Tim         int64  `json:"tim,omitempty"`
		Time        int    `json:"time"`
		Md5         string `json:"md5,omitempty"`
		Fsize       int    `json:"fsize,omitempty"`
		Resto       int    `json:"resto"`
		Bumplimit   int    `json:"bumplimit,omitempty"`
		Imagelimit  int    `json:"imagelimit,omitempty"`
		SemanticURL string `json:"semantic_url,omitempty"`
		Replies     int    `json:"replies,omitempty"`
		Images      int    `json:"images,omitempty"`
		UniqueIps   int    `json:"unique_ips,omitempty"`
		TailSize    int    `json:"tail_size,omitempty"`
	} `json:"posts"`
}

func main() {
	kingpin.Parse()
	t := &Thread{}
	GetThread(*thread, *board, &t)
	if _, err := os.Stat("./conf/app.ini"); err != nil {
		if os.IsNotExist(err) {
			os.Mkdir(*path, 755)
		} else {
			panic(err)
		}
	}
	bar := pb.StartNew(len(t.Posts))
	start := time.Now()
	for _, p := range t.Posts {
		url := GetPic(strconv.FormatInt(p.Tim, 10), p.Ext, *board)
		if url != "" {
			DownloadPic(strconv.FormatInt(p.Tim, 10), p.Ext, url)
		}
		bar.Increment()
	}
	c, _ := ioutil.ReadDir(*path)
	bar.FinishPrint("> Downloaded " + strconv.Itoa(len(c)) + " files in " + strconv.FormatFloat(time.Since(start).Seconds(), 'f', -1, 64) + " seconds")
}

// GetThread fetches a thread
func GetThread(thread, board string, target interface{}) {
	client := &http.Client{Timeout: 10 * time.Second}
	url := "https://a.4cdn.org/" + board + "/thread/" + thread + ".json"
	r, err := client.Get(url)
	if err != nil {
		panic(err)
	}
	defer r.Body.Close()
	json.NewDecoder(r.Body).Decode(target)
}

// GetPic generates the string
func GetPic(tim, ext, board string) string {
	if ext != "" && utf8.RuneCountInString(ext) == 4 {
		return "https://i.4cdn.org/" + board + "/" + tim + ext
	}
	return ""
}

// DownloadPic downloads and writes a file
func DownloadPic(tim, ext, url string) {
	out, err := os.Create(*path + "/" + tim + ext)
	if err != nil {
		panic(err)
	}
	defer out.Close()
	resp, err := http.Get(url)
	if err != nil || resp.StatusCode != 200 {
		log.Fatal("Failed to get ressource. Status: " + strconv.Itoa(resp.StatusCode) + " (Error: " + err.Error() + ")")
	}
	defer resp.Body.Close()
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		panic(err)
	}
}
